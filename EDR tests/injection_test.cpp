
#include <Windows.h>
#include <winerror.h>
#include <stdio.h>
#include <psapi.h>
#include <tchar.h>
#include <string.h>
#include <iostream>
#include <string>
#include <list>
#define _CRT_SECURE_NO_WARNINGS
#define DLL_NAME L"MyDLL.dll"
#define DLL_PATH  "E:\\מגשימים ארכיטקטורה\\week 17\\Idan\\MyDLL\\x64\\Release\\MyDLL.dll"

int dllInjection(DWORD target_pid);


// Function to eject a DLL from a target process
int dllInjection(DWORD target_pid)
{

    // Open the target process
    HANDLE hProcess = OpenProcess(PROCESS_CREATE_THREAD | PROCESS_VM_OPERATION | PROCESS_VM_WRITE, FALSE, target_pid);
    if (!hProcess)
    {
        std::cerr << "Failed to open process. Error code: " << GetLastError() << std::endl;
        return 1;
    }

    // Allocate memory in the remote process
    LPVOID lpHeapBaseAddress1 = VirtualAllocEx(hProcess, nullptr, sizeof(DLL_PATH), MEM_COMMIT, PAGE_READWRITE);
    if (!lpHeapBaseAddress1)
    {
        std::cerr << "Failed to allocate memory in remote process. Error code: " << GetLastError() << std::endl;
        CloseHandle(hProcess);
        return 1;
    }
    // Write the DLL path to the remote allocated memory
    SIZE_T bytesWritten;

    if (!WriteProcessMemory(hProcess, lpHeapBaseAddress1, DLL_PATH, sizeof(DLL_PATH), &bytesWritten))
    {
        std::cerr << "Failed to write to remote process memory. Error code: " << GetLastError() << std::endl;
        VirtualFreeEx(hProcess, lpHeapBaseAddress1, 0, MEM_RELEASE);
        CloseHandle(hProcess);
        return 1;
    }

    // Get the address of LoadLibrary in kernel32.dll
    LPTHREAD_START_ROUTINE lpLoadLibraryStartAddress = (LPTHREAD_START_ROUTINE)GetProcAddress(GetModuleHandle(L"Kernel32.dll"), "LoadLibraryA");
    if (!lpLoadLibraryStartAddress)
    {
        std::cerr << "Failed to get the address of LoadLibraryA. Error code: " << GetLastError() << std::endl;
        VirtualFreeEx(hProcess, lpHeapBaseAddress1, 0, MEM_RELEASE);
        CloseHandle(hProcess);
        return 1;
    }
    // Call LoadLibrary in the remote process
    HANDLE hThread = CreateRemoteThread(hProcess, nullptr, 0, lpLoadLibraryStartAddress, lpHeapBaseAddress1, 0, nullptr);
    if (!hThread)
    {
        std::cerr << "Failed to create remote thread. Error code: " << GetLastError() << std::endl;
        VirtualFreeEx(hProcess, lpHeapBaseAddress1, 0, MEM_RELEASE);
        CloseHandle(hProcess);
        return 1;
    }
    // Wait for the thread to complete
    std::cout << "HERE4" << std::endl;

    WaitForSingleObject(hThread, INFINITY);
    std::cout << "HERE5" << std::endl;

    // Clean up
    CloseHandle(hThread);
    VirtualFreeEx(hProcess, lpHeapBaseAddress1, 0, MEM_RELEASE);
    CloseHandle(hProcess);
    return 0;
}

int main()
{
    Sleep(5000);
    dllInjection(17556);

    return 1;
}