#include <Windows.h>
#include <winerror.h>
#include <stdio.h>
#include <psapi.h>
#include <tchar.h>
#include <string.h>
#include <iostream>
#include <string>
#include <list>
#include "pipeMonitor.h"

struct ProcessList
{
    DWORD* processes_ID_list;
    size_t size;
};
std::unordered_map<int, std::string> processMap;

int dllInjection(DWORD target_pid);
ProcessList* get_Processes_ID_List();
void PrintProcessNameAndID(DWORD processID);
