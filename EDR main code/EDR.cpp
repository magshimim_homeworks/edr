#define _CRT_SECURE_NO_WARNINGS
#include "EDR.h"
#define DLL_NAME L"EDR_DLL.dll"
#define DLL_PATH  "E:\\מגשימים ארכיטקטורה\\week 17\\Idan\\MyDLL\\x64\\Release\\EDR_DLL.dll"
#define TARGET_PROCESS_ID 10244
static std::list<LPCSTR> suspiciousProcesses;
static std::list<int> suspiciousProcessesPID;
static std::vector<std::string> systemProcessesNames = {
        "System Idle Process",
        "System",
        "smss.exe",
        "csrss.exe",
        "wininit.exe",
        "services.exe",
        "lsass.exe",
        "svchost.exe",
        "explorer.exe",
        "winlogon.exe",
        "taskhostw.exe",
        "dwm.exe",
        "spoolsv.exe",
        "taskeng.exe",
        "audiodg.exe",
        "conhost.exe",
        "wuauserv",
        "wuauclt.exe",
        "msiexec.exe",
        "notepad.exe",
        "mstsc.exe",
        "wmiprvse.exe",
        "searchindexer.exe",
        "sihost.exe",
        "fontdrvhost.exe",
        "runtimebroker.exe",
        "inetinfo.exe",
        "ctfmon.exe",
        "sppsvc.exe",
        "rundll32.exe",

        "StartMenuExperienceHost.exe",
        "ShellExperienceHost.exe",
        "ServiceHub.Host.CLR.x86.exe",
        "ServiceHub.Host.CLR.x64.exe",
        "PhoneExperienceHost.exe",
        "Microsoft.ServiceHub.Controller.exe",
        "SystemSettings.exe",
        "EDR.exe"
};



void scanProcesses();
std::string GetProcessPathFromID(DWORD processId);
std::string GetProcessNameFromPath(std::string ProcessPath);
int dllInjection(DWORD target_pid);


int dllInjection(DWORD target_pid)
{
    // Open the target process
    HANDLE hProcess = OpenProcess(PROCESS_CREATE_THREAD | PROCESS_VM_OPERATION | PROCESS_VM_WRITE, FALSE, target_pid);
    if (!hProcess)
    {
        std::cerr << "Failed to open process. Error code: " << GetLastError() << std::endl;
        return 1;
    }

    // Allocate memory in the remote process
    LPVOID lpHeapBaseAddress1 = VirtualAllocEx(hProcess, nullptr, strlen(DLL_PATH) + 1, MEM_COMMIT, PAGE_READWRITE);
    if (!lpHeapBaseAddress1)
    {
        std::cerr << "Failed to allocate memory in remote process. Error code: " << GetLastError() << std::endl;
        CloseHandle(hProcess);
        return 1;
    }

    // Write the DLL path to the remote allocated memory
    SIZE_T bytesWritten;
    if (!WriteProcessMemory(hProcess, lpHeapBaseAddress1, DLL_PATH, strlen(DLL_PATH) + 1, &bytesWritten))
    {
        std::cerr << "Failed to write to remote process memory. Error code: " << GetLastError() << std::endl;
        VirtualFreeEx(hProcess, lpHeapBaseAddress1, 0, MEM_RELEASE);
        CloseHandle(hProcess);
        return 1;
    }

    // Get the address of LoadLibraryA in kernel32.dll
    HMODULE hKernel32 = GetModuleHandle(L"Kernel32.dll");
    if (!hKernel32)
    {
        std::cerr << "Failed to get handle of Kernel32.dll. Error code: " << GetLastError() << std::endl;
        VirtualFreeEx(hProcess, lpHeapBaseAddress1, 0, MEM_RELEASE);
        CloseHandle(hProcess);
        return 1;
    }

    LPTHREAD_START_ROUTINE lpLoadLibraryStartAddress = (LPTHREAD_START_ROUTINE)GetProcAddress(hKernel32, "LoadLibraryA");
    if (!lpLoadLibraryStartAddress)
    {
        std::cerr << "Failed to get the address of LoadLibraryA. Error code: " << GetLastError() << std::endl;
        VirtualFreeEx(hProcess, lpHeapBaseAddress1, 0, MEM_RELEASE);
        CloseHandle(hProcess);
        return 1;
    }

    // Call LoadLibraryA in the remote process
    HANDLE hThread = CreateRemoteThread(hProcess, nullptr, 0, lpLoadLibraryStartAddress, lpHeapBaseAddress1, 0, nullptr);
    if (!hThread)
    {
        std::cerr << "Failed to create remote thread. Error code: " << GetLastError() << std::endl;
        VirtualFreeEx(hProcess, lpHeapBaseAddress1, 0, MEM_RELEASE);
        CloseHandle(hProcess);
        return 1;
    }

    // Wait for the thread to complete
    WaitForSingleObject(hThread, 100);

    // Clean up
    CloseHandle(hThread);
    VirtualFreeEx(hProcess, lpHeapBaseAddress1, 0, MEM_RELEASE);
    CloseHandle(hProcess);

    return 0;
}



void scanProcesses()
{
    DWORD procIDs[1024];
    DWORD bytesNeeded;
    if (!EnumProcesses(procIDs, sizeof(procIDs), &bytesNeeded))
    {
        std::cerr << "EnumProcesses failed with error code: " << GetLastError() << std::endl;
        return;
    }

    // Iterate through the process IDs
    for (DWORD i = 0; i < bytesNeeded / sizeof(DWORD); ++i)
    {
        if (procIDs[i] != 0)
        {
            bool isSystemProcess = false;
            std::string processPath = GetProcessPathFromID(procIDs[i]);


            std::string process_name = GetProcessNameFromPath(processPath);
            auto it = std::find(systemProcessesNames.begin(), systemProcessesNames.end(), process_name); // CHECK IF PROCESS IS A SYSTEM PROCESS


            if(it == systemProcessesNames.end() && processPath.find("C:\\Windows\\System32") == std::string::npos && process_name != "")
                processMap[procIDs[i]] = process_name;



        }
    }
}


std::string GetProcessPathFromID(DWORD processId)
{
    HANDLE hProcess = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, FALSE, processId);
    if (hProcess == NULL)
    {
        return "";
    }

    // Get the process image file name.
    char ProcessPath[MAX_PATH];
    DWORD size = sizeof(ProcessPath);
    if (QueryFullProcessImageNameA(hProcess, 0, ProcessPath, &size) == 0)
    {
        CloseHandle(hProcess);
        return "";
    }

    CloseHandle(hProcess);
    std::string pathString(ProcessPath);
    return pathString;
}


std::string GetProcessNameFromPath(std::string ProcessPath)
{
    if (ProcessPath == "")
        return "";
    int find_last = 0;
    CHAR slash = '\\';
    for (size_t i = 0; i < ProcessPath.length(); i++)
    {
        if (ProcessPath[i] == slash)
        {
            find_last = i;
        }
    }
    std::string ProcessName = &(ProcessPath[find_last + 1]);
    return ProcessName;
}



int main(int argc, char* argv[])
{
    int counter = 0;
    scanProcesses();
    std::cout << "welcome to Idan's EDR\n starting scanning processes...." << std::endl;

    for (const auto& pair : processMap)
    {
        counter++;

         std::cout << "PID: " << pair.first << ", Process Name: " << pair.second << std::endl;
         dllInjection(pair.first);
        //if (pair.second == "InjectionAndHookingTest2.exe")
        //{
        //    std::cout << "PID: " << pair.first << ", Process Name: " << pair.second << std::endl;
        //    dllInjection(pair.first);

        //}

    } 
    std::cout << "dll injected to all available processs" << std::endl;

    std::thread myThread(createPipeThreads, processMap);
    myThread.join();
    return 0;

}
