#include <iostream>
#include <map>
#include <list>
#include <string>
#include <mutex>
#include <unordered_map>
#include "response.h"

//map <int PID, list<string functionCalls>>
static std::map<int, std::list<std::string>> processesCallsMap;







static std::unordered_map<std::string, std::list<std::string>> suspicious_sequences = {
    {"DLL injection", {"GetProcAddress", "VirtualAllocEx", "CreateRemoteThread"}},
    {"hack", {"MessageBoxA","MessageBoxA","MessageBoxA"}},
    {"worm", {"CreateProcess","CreateRemoteThread","WriteProcessMemory"}}
};

void addCall(int PID, std::string callName);

//checks if every element of list2 is present in list1 in the same order, it returns true; otherwise, it returns false.
bool IsListInList(std::list<std::string > list1, std::list<std::string > list2);
std::string IsMaliciousProcess(int PID);
//clear the calls list of a given PID
void clearCallsList(int PID);