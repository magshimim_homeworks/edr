#include "response.h"


std::string GetProcessNameFromIDA(DWORD processId)
{
    HANDLE hProcess = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, FALSE, processId);
    if (hProcess == NULL)
    {
        return "";
    }

    // Get the process image file name.
    char ProcessPath[MAX_PATH];
    DWORD size = sizeof(ProcessPath);
    if (QueryFullProcessImageNameA(hProcess, 0, ProcessPath, &size) == 0)
    {
        CloseHandle(hProcess);
        return "";
    }

    CloseHandle(hProcess);

    int find_last = 0;
    CHAR slash = '\\';
    for (size_t i = 0; i < strlen(ProcessPath); i++)
    {
        if (ProcessPath[i] == slash)
        {
            find_last = i;
        }
    }
    std::string ProcessName = &(ProcessPath[find_last + 1]);
    return ProcessName;

}

void  response(int PID,std::string attack ) 
{
    std::string title = "Alert! " + attack + " detected!";
    std::string msg = "whould you like to kill  "+ GetProcessNameFromIDA(PID)+"  \nthis process is suspected of committing " + attack;
    //std::string msg = "whould you like to kill    \nthis process is suspected of committing " + attack;
    int result = MessageBoxA(nullptr, msg.c_str(), title.c_str(), MB_YESNO | MB_ICONQUESTION);
    if(result == IDYES)
    {
        killProcess(PID);

    }
    
}


void killProcess(int PID) 
{
    HANDLE hProcess = OpenProcess(PROCESS_TERMINATE, FALSE, PID);
    if (hProcess != NULL) {
        if (TerminateProcess(hProcess, 0))
        {
            std::cout << "Process with PID " << PID << " killed successfully." << std::endl;
        }
        else {
            std::cout << "Failed to kill process with PID " << PID << std::endl;
        }
        CloseHandle(hProcess);
    }
    else {
        std::cout << "Failed to open process with PID " << PID << std::endl;
    }
}


