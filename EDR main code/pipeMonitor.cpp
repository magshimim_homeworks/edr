#include "pipeMonitor.h"

static std::vector<std::thread> threads;

void createPipeThreads(const std::unordered_map<int, std::string> PIDMap)
{
	for (const auto& pair : PIDMap)
	{
        // Create the thread and detach it immediately
        threads.emplace_back(createPipe ,pair.first);
	}
    for (std::thread& thread : threads)
    {
        thread.join();
    }
}

void createPipe(const int PID)
{
    std::string pipeName = "\\\\.\\pipe\\Pipe" + std::to_string(PID);
    HANDLE pipeHandle = CreateNamedPipeA(
        pipeName.c_str(),
        PIPE_ACCESS_DUPLEX,
        PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT,
        PIPE_UNLIMITED_INSTANCES,
        1024,
        1024,
        0,
        NULL
    );

    if (pipeHandle == INVALID_HANDLE_VALUE)
    {
        std::cerr << "CreateNamedPipe PID:  "<<PID<< " failed.Error code : " << GetLastError() << std::endl;
        return;
    }

    pipeListener(pipeHandle, PID);
}
void pipeListener(HANDLE pipeHandle, int PID)
{
    while (pipeHandle != INVALID_HANDLE_VALUE)
    {
        char buffer[1024];
        DWORD bytesRead = 0;
        if (ConnectNamedPipe(pipeHandle, NULL) != FALSE)   // wait for someone to connect to the pipe
        {
            if(ReadFile(pipeHandle, buffer, sizeof(buffer) - 1, &bytesRead, NULL) != FALSE)
            {
                /* add terminating zero */
                buffer[bytesRead] = '\0';
                std::string functionCallName(buffer);
                addCall(PID, functionCallName);
                std::cout << "msg from: " << PID << "says: " << buffer << std::endl;
            }
        }

        DisconnectNamedPipe(pipeHandle);
    }

}
