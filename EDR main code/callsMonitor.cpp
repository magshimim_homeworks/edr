#include "callsMonitor.h"
std::mutex MapMutex;



void addCall(int PID, std::string callName)
{
	std::cout << "Process: " << PID << "executed function: " << callName << std::endl;
    {
        std::lock_guard<std::mutex> lock(MapMutex);
        processesCallsMap[PID].push_back(callName);

        std::string malicious = IsMaliciousProcess(PID);
        if (malicious != "false")
        {
            std::cout << "+++++++++++++process " << PID << " is sus of: " << malicious << std::endl;
            clearCallsList(PID);
            response(PID, malicious);
        }
    }

}
void clearCallsList(int PID)
{
    
        // Check if the key exists in the map
        auto it = processesCallsMap.find(PID);
        if (it != processesCallsMap.end()) {
            //std::lock_guard<std::mutex> lock(MapMutex);

            // Clear the list associated with the key
            it->second.clear();
        }
    
}
//this function returns true if list1 contains list2 (in the same order), and false otherwise.
bool IsListInList(std::list<std::string > list1, std::list<std::string > list2)
{
    auto it1 = list1.begin();
    auto it2 = list2.begin();

    while (it1 != list1.end() && it2 != list2.end()) {
        if (*it1 == *it2) {
            ++it1;
            ++it2;
        }
        else {
            ++it1;
        }
    }

    return it2 == list2.end(); // If it2 reached the end, list2 is completely matched in list1
}

//The function goes through the suspicious sequences and checks if one of the sequences was executed in the call list of the process
//if not return string(false) else return the malicious attack name


std::string IsMaliciousProcess(int PID)
{
    for (auto it = suspicious_sequences.begin(); it != suspicious_sequences.end(); ++it)
    {
        if (IsListInList(processesCallsMap[PID], it->second))
        {
            return it->first;
        }

    }
    return "false";

}