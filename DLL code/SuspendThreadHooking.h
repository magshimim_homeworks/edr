#pragma once
#pragma once
#include <iostream>
#include <windows.h>

#ifdef DLL_EXPORT //checks id the macro is defined
#define DECLDIR __declspec(dllexport)
#else
#define DECLDIR __declspec(dllimport)
#endif
// Declare additional functions
extern "C"
{
	DECLDIR void HookSuspendThread();
	DECLDIR DWORD HookedSuspendThread(
		HANDLE hThread);
	DECLDIR void UnHookSuspendThread();
#include "HookingStruct.h";

}