#pragma once
#include <windows.h>
#ifdef DLL_EXPORT //checks id the macro is defined
#define DECLDIR __declspec(dllexport)
#else
#define DECLDIR __declspec(dllimport)
#endif
// Declare additional functions
extern "C"
{
    struct HookedFunction
    {
        char hookedFunctionName[50];
        char functionOriginalBytes[12];
        PVOID functionOriginalAddress;
        SIZE_T bytesRead = 0;
        SIZE_T bytesWritten = 0;
        int modifyTimes = 0;
    };



}