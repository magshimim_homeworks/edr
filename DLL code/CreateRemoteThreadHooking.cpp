#include "CreateRemoteThreadHooking.h"
#include "CallsInformer.cpp"

static HookedFunction* CreateRemoteThreadHookedStruct = NULL;

HANDLE HookedCreateRemoteThread(
    HANDLE                 hProcess,
    LPSECURITY_ATTRIBUTES  lpThreadAttributes,
    SIZE_T                 dwStackSize,
    LPTHREAD_START_ROUTINE lpStartAddress,
    LPVOID                 lpParameter,
    DWORD                  dwCreationFlags,
    LPDWORD                lpThreadId)
{

    // print intercepted values from the MessageBoxA function
    std::cout << "Ohai from the  HookedCreateRemoteThread function\n" << "args: non relevant" << std::endl;

    sendMessageToPipe("CreateRemoteThread");
    UnHookCreateRemoteThread();
    HANDLE returnVal = CreateRemoteThread(hProcess,lpThreadAttributes,dwStackSize,lpStartAddress,lpParameter,dwCreationFlags,lpThreadId);
    HookCreateRemoteThread();

    return returnVal;
}

void UnHookCreateRemoteThread()
{
    // unpatch CreateRemoteThread
    if (CreateRemoteThreadHookedStruct != NULL)
    {
        WriteProcessMemory(GetCurrentProcess(),
            (LPVOID)CreateRemoteThreadHookedStruct->functionOriginalAddress,
            CreateRemoteThreadHookedStruct->functionOriginalBytes,
            sizeof(CreateRemoteThreadHookedStruct->functionOriginalBytes), NULL);
    }

}


void HookCreateRemoteThread()
{
    printf("hooking... HookCreateRemoteThread\n");
    if (CreateRemoteThreadHookedStruct == NULL)
    {
        std::cout << "HERE 1 HookCreateRemoteThread" << std::endl;

        CreateRemoteThreadHookedStruct = new HookedFunction;
    }



    // Get the handle to user32.dll (if it's already loaded)
    HMODULE library = GetModuleHandle(L"kernel32.dll");
    if (library == NULL)
    {
        std::cout << "Handle error - couldn't get the address: " << GetLastError() << std::endl;
    }
    else
    {
        if (CreateRemoteThreadHookedStruct->modifyTimes == 0)
        {
            std::cout << "HERE 2" << std::endl;

            FARPROC CreateRemoteThreadAddress = GetProcAddress(GetModuleHandle(L"kernel32.dll"), "CreateRemoteThread");
            CreateRemoteThreadHookedStruct->functionOriginalAddress = CreateRemoteThreadAddress;
            // save the first 6 bytes of the original MessageBoxA function - will need for unhooking
            ReadProcessMemory(GetCurrentProcess(), CreateRemoteThreadAddress, CreateRemoteThreadHookedStruct->functionOriginalBytes, 12, &CreateRemoteThreadHookedStruct->bytesRead);
            CreateRemoteThreadHookedStruct->modifyTimes = 1;
        }



        // create a patch "movabs rax, <address of new MessageBoxA>; jmp rax"
        unsigned char patch[] =
        {
            0x48, 0xb8,             // movabs rax,
            0x00,0x00, 0x00, 0x00,  // <address of hook>
            0x00, 0x00, 0x00, 0x00, // <address of hook>
            0xFF, 0xE0              // jmp rax
        };
        *(DWORD64*)&patch[2] = (DWORD64)HookedCreateRemoteThread;

        DWORD oldprot;
        if (WriteProcessMemory(GetCurrentProcess(), (LPVOID)CreateRemoteThreadHookedStruct->functionOriginalAddress, patch, sizeof(patch), NULL))
        {
            printf("Hooked successfully\n");
        }
        std::cout << "finished hooking...\n\n" << std::endl;
    }
}