#pragma once
#include <iostream>
#include <windows.h>
#ifdef DLL_EXPORT //checks id the macro is defined
#define DECLDIR __declspec(dllexport)
#else
#define DECLDIR __declspec(dllimport)
#endif
// Declare additional functions
extern "C"
{
	DECLDIR void HookVirtualAllocEx();
	DECLDIR LPVOID HookedVirtualAllocEx(HANDLE hProcess,LPVOID lpAddress,SIZE_T dwSize,DWORD  flAllocationType,DWORD  flProtect);
	DECLDIR void UnHookVirtualAllocEx();
#include "HookingStruct.h";

}