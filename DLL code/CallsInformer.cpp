#include "CallsInformer.h"

bool sendMessageToPipe(LPCSTR procName)
{

    int currentProcessId = GetCurrentProcessId();
    // Connect to named pipe
    std::string pipeName = "\\\\.\\pipe\\Pipe" + std::to_string(currentProcessId);
    HANDLE pipeHandle = CreateFileA(
        pipeName.c_str(),
        GENERIC_READ | GENERIC_WRITE,
        0,
        NULL,
        OPEN_EXISTING,
        0,
        NULL
    );

    if (pipeHandle == INVALID_HANDLE_VALUE) {
        std::cerr << "Failed to connect to named pipe. Error code: " << GetLastError() << std::endl;
        return 1;
    }
    // Send message to pipe
    DWORD dwWritten;
    if (!WriteFile(pipeHandle, procName, strlen(procName), &dwWritten, NULL))
    {
        std::cerr << "Failed to write to pipe. Error code: " << GetLastError() << std::endl;
    }
    else
    {
        std::cout << "Message sent successfully.: " << procName << std::endl;
    }

    // Close pipe
    CloseHandle(pipeHandle);

    return 1;

}

