#pragma once
#include <iostream>
#include <windows.h>

#ifdef DLL_EXPORT //checks id the macro is defined
#define DECLDIR __declspec(dllexport)
#else
#define DECLDIR __declspec(dllimport)
#endif
// Declare additional functions
extern "C"
{
	DECLDIR void HookGetProcAddress();
	DECLDIR FARPROC HookedGetProcAddress(HMODULE hModule, LPCSTR  lpProcName);
	DECLDIR void UnHookGetProcAddress();
	#include "HookingStruct.h";

}