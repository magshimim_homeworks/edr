#pragma once
#pragma once
#include <iostream>
#include <windows.h>

#ifdef DLL_EXPORT // Check if the macro is defined
#define DECLDIR __declspec(dllexport)
#else
#define DECLDIR __declspec(dllimport)
#endif

// Declare additional functions
extern "C"
{
    DECLDIR void HookSetThreadContext();
    DECLDIR BOOL HookedSetThreadContext(HANDLE hThread, const CONTEXT* lpContext);
    DECLDIR void UnHookSetThreadContext();
#include "HookingStruct.h";
}
