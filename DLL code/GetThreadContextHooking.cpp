#include "GetThreadContextHooking.h"
#include "CallsInformer.cpp"

static HookedFunction* GetThreadContextHookedStruct = NULL;

BOOL HookedGetThreadContext(HANDLE hThread, LPCONTEXT lpContext)
{
    // Print intercepted values
    std::cout << "Ohai from the HookedGetThreadContext function\n";

    sendMessageToPipe("GetThreadContext");

    UnHookGetThreadContext();
    BOOL returnVal = GetThreadContext(hThread, lpContext);
    HookGetThreadContext();

    return returnVal;
}

void UnHookGetThreadContext()
{
    // Unpatch GetThreadContext
    if (GetThreadContextHookedStruct != NULL)
    {
        WriteProcessMemory(GetCurrentProcess(),
            (LPVOID)GetThreadContextHookedStruct->functionOriginalAddress,
            GetThreadContextHookedStruct->functionOriginalBytes,
            sizeof(GetThreadContextHookedStruct->functionOriginalBytes), NULL);
    }
}

void HookGetThreadContext()
{
    printf("hooking... HookGetThreadContext\n");
    if (GetThreadContextHookedStruct == NULL)
    {
        std::cout << "HERE 1 HookGetThreadContext" << std::endl;

        GetThreadContextHookedStruct = new HookedFunction;
    }

    HMODULE library = GetModuleHandle(L"kernel32.dll");
    if (library == NULL)
    {
        std::cout << "Handle error - couldn't get the address: " << GetLastError() << std::endl;
    }
    else
    {
        if (GetThreadContextHookedStruct->modifyTimes == 0)
        {
            std::cout << "HERE 2" << std::endl;

            FARPROC GetThreadContextAddress = GetProcAddress(library, "GetThreadContext");
            GetThreadContextHookedStruct->functionOriginalAddress = GetThreadContextAddress;

            // Save the first 12 bytes of the original GetThreadContext function - will need for unhooking
            ReadProcessMemory(GetCurrentProcess(), GetThreadContextAddress, GetThreadContextHookedStruct->functionOriginalBytes, 12, &GetThreadContextHookedStruct->bytesRead);
            GetThreadContextHookedStruct->modifyTimes = 1;
        }

        unsigned char patch[] =
        {
            0x48, 0xb8,             // movabs rax,
            0x00,0x00, 0x00, 0x00,  // <address of hook>
            0x00, 0x00, 0x00, 0x00, // <address of hook>
            0xFF, 0xE0              // jmp rax
        };
        *(DWORD64*)&patch[2] = (DWORD64)HookedGetThreadContext;

        if (WriteProcessMemory(GetCurrentProcess(), (LPVOID)GetThreadContextHookedStruct->functionOriginalAddress, patch, sizeof(patch), NULL))
        {
            printf("Hooked successfully\n");
        }
        std::cout << "finished hooking...\n\n" << std::endl;
    }
}
