#include "SetThreadContextHooking.h"
#include "CallsInformer.cpp"

static HookedFunction* SetThreadContextHookedStruct = NULL;

BOOL HookedSetThreadContext(HANDLE hThread, const CONTEXT* lpContext)
{
    // Print intercepted values
    std::cout << "Ohai from the HookedSetThreadContext function\n";

    sendMessageToPipe("SetThreadContext");

    UnHookSetThreadContext();
    BOOL returnVal = SetThreadContext(hThread, lpContext);
    HookSetThreadContext();

    return returnVal;
}

void UnHookSetThreadContext()
{
    // Unpatch SetThreadContext
    if (SetThreadContextHookedStruct != NULL)
    {
        WriteProcessMemory(GetCurrentProcess(),
            (LPVOID)SetThreadContextHookedStruct->functionOriginalAddress,
            SetThreadContextHookedStruct->functionOriginalBytes,
            sizeof(SetThreadContextHookedStruct->functionOriginalBytes), NULL);
    }
}

void HookSetThreadContext()
{
    printf("hooking... HookSetThreadContext\n");
    if (SetThreadContextHookedStruct == NULL)
    {
        std::cout << "HERE 1 HookSetThreadContext" << std::endl;

        SetThreadContextHookedStruct = new HookedFunction;
    }

    HMODULE library = GetModuleHandle(L"kernel32.dll");
    if (library == NULL)
    {
        std::cout << "Handle error - couldn't get the address: " << GetLastError() << std::endl;
    }
    else
    {
        if (SetThreadContextHookedStruct->modifyTimes == 0)
        {
            std::cout << "HERE 2" << std::endl;

            FARPROC SetThreadContextAddress = GetProcAddress(library, "SetThreadContext");
            SetThreadContextHookedStruct->functionOriginalAddress = SetThreadContextAddress;

            // Save the first 12 bytes of the original SetThreadContext function - will need for unhooking
            ReadProcessMemory(GetCurrentProcess(), SetThreadContextAddress, SetThreadContextHookedStruct->functionOriginalBytes, 12, &SetThreadContextHookedStruct->bytesRead);
            SetThreadContextHookedStruct->modifyTimes = 1;
        }

        unsigned char patch[] =
        {
            0x48, 0xb8,             // movabs rax,
            0x00,0x00, 0x00, 0x00,  // <address of hook>
            0x00, 0x00, 0x00, 0x00, // <address of hook>
            0xFF, 0xE0              // jmp rax
        };
        *(DWORD64*)&patch[2] = (DWORD64)HookedSetThreadContext;

        if (WriteProcessMemory(GetCurrentProcess(), (LPVOID)SetThreadContextHookedStruct->functionOriginalAddress, patch, sizeof(patch), NULL))
        {
            printf("Hooked successfully\n");
        }
        std::cout << "finished hooking...\n\n" << std::endl;
    }
}
