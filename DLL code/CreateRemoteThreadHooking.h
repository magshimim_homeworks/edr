#pragma once
#include <iostream>
#include <windows.h>

#ifdef DLL_EXPORT //checks id the macro is defined
#define DECLDIR __declspec(dllexport)
#else
#define DECLDIR __declspec(dllimport)
#endif
// Declare additional functions
extern "C"
{
	DECLDIR void HookCreateRemoteThread();
	DECLDIR HANDLE HookedCreateRemoteThread(
		 HANDLE                 hProcess,
		 LPSECURITY_ATTRIBUTES  lpThreadAttributes,
		 SIZE_T                 dwStackSize,
		 LPTHREAD_START_ROUTINE lpStartAddress,
		 LPVOID                 lpParameter,
		 DWORD                  dwCreationFlags,
		 LPDWORD                lpThreadId);
	DECLDIR void UnHookCreateRemoteThread();
#include "HookingStruct.h";

}