#include "SuspendThreadHooking.h"
#include "CallsInformer.cpp"

static HookedFunction* SuspendThreadHookedStruct = NULL;

DWORD WINAPI HookedSuspendThread(HANDLE hThread)
{
    // print intercepted values from the SuspendThread function
    std::cout << "Ohai from the HookedSuspendThread function\n" << "args: " << hThread << std::endl;

    sendMessageToPipe("SuspendThread");

    UnHookSuspendThread();
    DWORD returnVal = SuspendThread(hThread);
    HookSuspendThread();

    return returnVal;
}

void UnHookSuspendThread()
{
    // unpatch SuspendThread
    if (SuspendThreadHookedStruct != NULL)
    {
        WriteProcessMemory(GetCurrentProcess(),
            (LPVOID)SuspendThreadHookedStruct->functionOriginalAddress,
            SuspendThreadHookedStruct->functionOriginalBytes,
            sizeof(SuspendThreadHookedStruct->functionOriginalBytes), NULL);
    }
}

void HookSuspendThread()
{
    printf("hooking... HookSuspendThread\n");
    if (SuspendThreadHookedStruct == NULL)
    {
        std::cout << "HERE 1 HookSuspendThread" << std::endl;

        SuspendThreadHookedStruct = new HookedFunction;
    }

    // Get the handle to kernel32.dll (if it's already loaded)
    HMODULE library = GetModuleHandle(L"kernel32.dll");
    if (library == NULL)
    {
        std::cout << "Handle error - couldn't get the address: " << GetLastError() << std::endl;
    }
    else
    {
        if (SuspendThreadHookedStruct->modifyTimes == 0)
        {
            std::cout << "HERE 2" << std::endl;

            FARPROC SuspendThreadAddress = GetProcAddress(library, "SuspendThread");
            SuspendThreadHookedStruct->functionOriginalAddress = SuspendThreadAddress;
            // save the first bytes of the original SuspendThread function - will need for unhooking
            ReadProcessMemory(GetCurrentProcess(), SuspendThreadAddress, SuspendThreadHookedStruct->functionOriginalBytes, 12, &SuspendThreadHookedStruct->bytesRead);
            SuspendThreadHookedStruct->modifyTimes = 1;
        }

        // create a patch "movabs rax, <address of new SuspendThread>; jmp rax"
        unsigned char patch[] =
        {
            0x48, 0xb8,             // movabs rax,
            0x00,0x00, 0x00, 0x00,  // <address of hook>
            0x00, 0x00, 0x00, 0x00, // <address of hook>
            0xFF, 0xE0              // jmp rax
        };
        *(DWORD64*)&patch[2] = (DWORD64)HookedSuspendThread;

        DWORD oldprot;
        if (WriteProcessMemory(GetCurrentProcess(), (LPVOID)SuspendThreadHookedStruct->functionOriginalAddress, patch, sizeof(patch), NULL))
        {
            printf("Hooked successfully\n");
        }
        std::cout << "finished hooking...\n\n" << std::endl;
    }
}
