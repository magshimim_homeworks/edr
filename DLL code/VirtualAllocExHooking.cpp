#include "VirtualAllocExHooking.h"
#include "CallsInformer.cpp"

static HookedFunction* VirtualAllocExStruct = NULL;

LPVOID HookedVirtualAllocEx(HANDLE hProcess, LPVOID lpAddress, SIZE_T dwSize, DWORD  flAllocationType, DWORD  flProtect)
{

    // print intercepted values from the MessageBoxA function
    printf("Ohai from the HookedVirtualAllocEx function\n");
    sendMessageToPipe("VirtualAllocEx");
    UnHookVirtualAllocEx();
    LPVOID returnVal = VirtualAllocEx( hProcess,  lpAddress,  dwSize,   flAllocationType,   flProtect);
    HookVirtualAllocEx();

    return returnVal;
}

void UnHookVirtualAllocEx()
{
    // unpatch MessageBoxA
    if (VirtualAllocExStruct != NULL)
    {
        WriteProcessMemory(GetCurrentProcess(),
            (LPVOID)VirtualAllocExStruct->functionOriginalAddress,
            VirtualAllocExStruct->functionOriginalBytes,
            sizeof(VirtualAllocExStruct->functionOriginalBytes), NULL);
    }

}

void HookVirtualAllocEx()
{
    printf("hooking...\n");

    VirtualAllocExStruct = new HookedFunction;
    VirtualAllocExStruct->bytesRead = 0;
    VirtualAllocExStruct->bytesWritten = 0;

    // Get the handle to user32.dll (if it's already loaded)
    HMODULE library = GetModuleHandle(L"kernel32.dll");
    if (library == NULL)
    {
        std::cout << "Handle error - couldn't get the address: " << GetLastError() << std::endl;
    }
    else
    {
        if (VirtualAllocExStruct->modifyTimes == 0)
        {
            FARPROC VirtualAllocExAddress = GetProcAddress(GetModuleHandle(L"kernel32.dll"), "VirtualAllocEx");
            // Check if the address is valid
            if (VirtualAllocExAddress == NULL)
            {
                std::cout << "Failed to get address of VirtualAllocEx: " << GetLastError() << std::endl;
                return;  // Or handle the error accordingly
            }
            VirtualAllocExStruct->functionOriginalAddress = VirtualAllocExAddress;
            // save the first 6 bytes of the original MessageBoxA function - will need for unhooking
            ReadProcessMemory(GetCurrentProcess(), VirtualAllocExAddress, VirtualAllocExStruct->functionOriginalBytes, 12, &VirtualAllocExStruct->bytesRead);

            VirtualAllocExStruct->modifyTimes = 1;
        }

        // create a patch "movabs rax, <address of new MessageBoxA>; jmp rax"
        unsigned char patch[] =
        {
            0x48, 0xb8,             // movabs rax,
            0x00,0x00, 0x00, 0x00,  // <address of hook>
            0x00, 0x00, 0x00, 0x00, // <address of hook>
            0xFF, 0xE0              // jmp rax
        };
        *(DWORD64*)&patch[2] = (DWORD64)HookedVirtualAllocEx;

        DWORD oldprot;
        if (WriteProcessMemory(GetCurrentProcess(), (LPVOID)VirtualAllocExStruct->functionOriginalAddress, patch, sizeof(patch), &VirtualAllocExStruct->bytesWritten))
        {
            printf("Hooked successfully\n");
        }
        std::cout << "finished hooking...\n\n" << std::endl;
    }
}